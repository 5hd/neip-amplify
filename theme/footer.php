<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package fivehdstarter
 */

?>

  </div><!-- #content -->

  <footer id="colophon" class="site-footer">
    <div class="site-info">
    	<p>©2021 New England Innocence Project</p>
<p><a href="">Terms of Service</a> - <a href="">Privacy Policy</a></p>
    </div><!-- .site-info -->
  </footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
