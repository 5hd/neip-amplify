<section id="community-convo" class="community no-pad-bottom">
	<h2 class="section-title">Community Conversation</h2>
	<div class="container no-marg-bottom">
		<div class="centered">
			<p>Join us immediately following the event for a community conversation with area exonerees:</p>
	<a href="https://us06web.zoom.us/j/86847300757" target="_blank" class="button">Click here to join the Community Conversation</a>
</div>
	<img src="/wp-content/uploads/2021/10/new-banner.jpg" style="width:100%;"/>
</div>
</section>