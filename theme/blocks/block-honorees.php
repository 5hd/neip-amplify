<section class="honorees menu-section no-pad-bottom" id="honorees">
	<h2 class="section-title">Honorees</h2>
	<div class="container">
	<div class="row">
		<div class="column">
			<img src="<?php block_field( 'logo-1' ); ?>" class="honoree-logo"/>
			<?php block_field( 'copy-1' ); ?>
			<img src="<?php block_field( 'headshots-1' ); ?>"/>
		</div>
		<div class="column">
			<img src="<?php block_field( 'logo-2' ); ?>" class="honoree-logo"/>
			<?php block_field( 'copy-2' ); ?>
			<img src="<?php block_field( 'headshots-2' ); ?>"/>
		</div>
	</div>
</div>
</section>