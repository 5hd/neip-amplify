<section class="sponsorship menu-section" id="program-book">
	<h2 class="section-title">Program Book</h2>
	<div class="container centered white no-marg-bottom">
		<p>View our Program Book</p>

		<div style="position:relative;padding-top:max(60%,326px);height:0;width:100%"><iframe sandbox="allow-top-navigation allow-top-navigation-by-user-activation allow-downloads allow-scripts allow-same-origin allow-popups allow-modals allow-popups-to-escape-sandbox" allowfullscreen="true" style="position:absolute;border:none;width:100%;height:100%;left:0;right:0;top:0;bottom:0;" src="https://e.issuu.com/embed.html?d=neip_voia_program_2021_final&u=newenglandinnocenceproject"></iframe></div>

		<a href="/wp-content/uploads/2021/09/Amplify-Sponsorship-Kit_Final-2.pdf" target="_blank" class="button" style="margin-top:20px;">Download our 2021 Sponsor Packet</a>


		<p style="max-width:900px; margin:auto; margin-top:20px;">For more information, to become a sponsor, or to learn about sponsorship opportunities, contact Jordan Salvatoriello at <a href="mailto:jordan@newenglandinnocence.org.">jordan@newenglandinnocence.org.</a></p>
	</div>
</section>