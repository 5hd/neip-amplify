<section class="registration menu-section" id="register">
	<div class="row">
		<div class="column">
			<h2 class="section-title white">Register</h2>
			<div class="container no-marg-bottom white">
				<?php block_field( 'copy' ); ?>
				<a href="https://secure.actblue.com/donate/voi2021register" class="button" target="popup" onclick="window.open('https://secure.actblue.com/donate/voi2021register','popup','width=600,height=600'); return false;">Register Online</a>
		</div>
		</div>
		<div class="column full-image">
		</div>
	</div>
</section>