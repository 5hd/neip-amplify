<section class="information menu-section" id="information">
	<div class="row">
		<div class="column">
			<h2 class="section-title"><?php block_field( 'title' ); ?></h2>
			<div class="container no-marg-bottom">
				<?php block_field( 'copy' ); ?>
		</div>
		</div>
		<div class="column full-image" style="background-image:url('<?php block_field( 'image' ); ?>')">
		</div>
	</div>
</section>