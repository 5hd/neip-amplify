		<section class="donate menu-section icon-section no-pad-bottom" id="donate">
			<h2 class="section-title">Make a Gift</h2>
			<div class="row icon-set">
				<div class="column three light-blue icon-box">
					<div class="icon">
						<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 432.31 281"><defs><style>.cls-1{fill:#fff}</style></defs><path class="cls-1" d="M90.35 493.77A43.35 43.35 0 00133.58 537h345.84a43.35 43.35 0 0043.23-43.23V382.93H90.35zM153.12 449h153.17v25.94H153.12zM479.42 256H133.58a43.35 43.35 0 00-43.23 43.23v23.17h432.3v-23.17A43.35 43.35 0 00479.42 256z" transform="translate(-90.35 -256)"/></svg>
					</div>
					<a href="https://secure.actblue.com/donate/voi2021amplify" class="button" target="popup" onclick="window.open('https://secure.actblue.com/donate/voi2021amplify','popup','width=600,height=600'); return false;">Donate Today</a>
				</div>
				<div class="column three dark-blue icon-box">
					<div class="icon">
						<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 395.82 311"><defs><style>.cls-1{fill:#fff}</style></defs><path class="cls-1" d="M476.14 241H136.86a28.3 28.3 0 00-28.27 28.27v212.05a28.3 28.3 0 0028.27 28.27h141.37v28.27h-35.34a7.07 7.07 0 100 14.14h127.22a7.07 7.07 0 000-14.14h-35.34v-28.27h141.37a28.3 28.3 0 0028.27-28.27v-212A28.3 28.3 0 00476.14 241zm-155.5 296.86h-28.28v-28.27h28.28zm169.63-56.54a14.15 14.15 0 01-14.13 14.13H136.86a14.15 14.15 0 01-14.13-14.13v-212a14.15 14.15 0 0114.13-14.13h339.28a14.15 14.15 0 0114.13 14.13z" transform="translate(-108.59 -241)"/><path class="cls-1" d="M406.66 412.12l19.79-8.48a7.07 7.07 0 00-.3-13.12l-96-36a7.07 7.07 0 00-9.1 9.1l36 96a7.06 7.06 0 0013.11.3l8.49-19.79 25 25a7.06 7.06 0 0010 0l18-18a7.07 7.07 0 000-10zm2 38l-27.38-27.39a7.06 7.06 0 00-5-2.07 7.48 7.48 0 00-1.33.12 7.07 7.07 0 00-5.16 4.17l-5.67 13.21-24.34-64.92 64.92 24.34-13.21 5.66a7.06 7.06 0 00-2.21 11.5l27.38 27.38zM250 354.09h28.28a7.07 7.07 0 000-14.14h-14.19v-14.13a7.07 7.07 0 10-14.14 0V340a21.21 21.21 0 000 42.41h14.14a7.07 7.07 0 110 14.14h-28.27a7.07 7.07 0 000 14.14H250v14.13a7.07 7.07 0 0014.14 0v-14.18a21.21 21.21 0 100-42.41H250a7.07 7.07 0 010-14.14zM440.8 297.55h-70.69a7.07 7.07 0 000 14.13h70.69a7.07 7.07 0 000-14.13zM440.8 340h-70.69a7.07 7.07 0 000 14.14h70.69a7.07 7.07 0 000-14.14z" transform="translate(-108.59 -241)"/><path class="cls-1" d="M327.31 421.07a7.1 7.1 0 00-9.95 1 77.78 77.78 0 116.27-88.93 7.07 7.07 0 0012.12-7.28A91.84 91.84 0 10328.34 431a7.08 7.08 0 00-1.03-9.93z" transform="translate(-108.59 -241)"/></svg>
					</div>
					<script>var _msdaf_id= '9f3be50bee033'</script>
         <script src='https://app.dafwidget.com/api/js/source.js'></script>

				</div>
				<div class="column three light-blue icon-box">
					<div class="icon">
						<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 443.79 388.66"><defs><style>.cls-1{fill:#fff}</style></defs><path class="cls-1" d="M387.8 371.15a68 68 0 10-68-68 68 68 0 0068 68zm-4.8-62.91c-11.57-3.69-19.46-8.81-19.46-18.74 0-9.11 6.73-16.17 18.27-18.21v-9.73h10.59v9a41 41 0 0115.73 3.44l-3.23 11.56a35.22 35.22 0 00-15.58-3.59c-7 0-9.27 3-9.27 5.84 0 3.28 3.77 5.53 13.05 8.71 12.89 4.3 18 9.92 18 19.24 0 9.1-6.71 16.88-19.15 18.83v10.54h-10.7v-9.72c-7.25-.31-14.27-2.26-18.37-4.42l3.27-12a39.44 39.44 0 0017.85 4.5c6.27 0 10.48-2.35 10.48-6.36S391 310.8 383 308.24zM272 266.23a32.12 32.12 0 10-32.12-32.11A32.11 32.11 0 00272 266.23z" transform="translate(-84.43 -202)"/><path class="cls-1" d="M521 364c-11.62-10.45-27.6-7.57-38.87 4.27-12 12.55-79.12 83.25-79.12 83.25-9 9.16-19.52 18.29-36 18.29h-73.5c-6.65 0-13.74-5.25-13.74-13.42a13.44 13.44 0 0113.74-13.65h67.41c11.58 0 23.84-9.33 23.84-24s-12.45-23.91-25-23.91H230.35c-29.73 0-45.22 9.81-67 29.26-4.86 4.35-11.12 9.89-17.7 15.71l81.86 88.54a16.35 16.35 0 0110.09-3h116.13c27.78 0 53-3.34 76.29-28.29 22.83-24.46 76.91-83 89.72-96.95 9.44-10.29 12.53-25.96 1.26-36.1zM128.92 443a8.17 8.17 0 00-11.55-.46l-30.32 28a8.18 8.18 0 00-.45 11.55l99.89 108.05 11.55.45 30.32-28a8.17 8.17 0 00.45-11.55z" transform="translate(-84.43 -202)"/></svg>
					</div>
					<a class="button launch-form show">Make a Pledge</a>
					<?php get_template_part( 'template-parts/content', 'modal-form' );?>
				</div>
			</div>
		</section>