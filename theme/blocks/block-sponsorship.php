<section id="sponsors" class="our-sponsors">
	<h2 class="section-title">Our Sponsors</h2>
	<div class="container centered">
		<span class="thank-you">THANK YOU TO OUR SPONSORS</span>
	</div>
	<div class="sponsor-type">
		<div class="container centered">
			<h3>Presenting Sponsors</h3>
			<ul class="logo-list">
				<li><img src="/wp-content/uploads/2021/10/BS_Primary_FC_RGB.jpg" class="sponsor-image"></li>
				<li><img src="/wp-content/uploads/2021/10/town-faire-logo-2.png" class="sponsor-image"></li>
			</ul>
		</div>
	</div>
	<div class="sponsor-type">
		<div class="container centered">
			<h3>Justice Champions</h3>
			<ul class="logo-list side-by-side">
				<li><img src="/wp-content/uploads/2021/10/FH_logo_hi-res_trans_bkgnd.png" class="sponsor-image"></li>
				<li><img src="/wp-content/uploads/2021/09/gardiner-howland.png" class="sponsor-image"></li>
				<li><img src="/wp-content/uploads/2021/10/Goodwin-logo.png" class="sponsor-image"></li>
				<li><img src="/wp-content/uploads/2021/10/LW_Sponsorships_RED-scaled.jpg" class="sponsor-image"></li>
				<li><img src="/wp-content/uploads/2021/10/ropes-and-gray-logo.png" class="sponsor-image"></li>
				<li><img src="/wp-content/uploads/2021/09/Rubius_Logo.svg" class="sponsor-image"></li>
			</ul>
		</div>
	</div>
	<div class="sponsor-type">
		<div class="container centered">
			<h3>Freedom Contributors</h3>
			<ul class="typed-sponsor">
				<li>Arent Fox LLP</li>
				<li>Brown Rudnick</li>
				<li>Loevy & Loevy Attorneys at Law</li>
				<li>McDermott Will & Emery LLP</li>
				<li>Morgan Lewis</li>
				<li>Skadden, Arps, Slate, Meagher & Flom LLP</li>
				<li>White & Case</li>
			</ul>
		</div>
	</div>
	<div class="sponsor-type">
		<div class="container centered">
			<h3>Hope Supporters</h3>
			<ul class="typed-sponsor">
				<li>Choate, Hall & Stewart LLP</li>
				<li>Deloitte</li>
				<li>Elkins, Auer, Rudof, & Schiff</li>
				<li>Gail Roberts, Ed Feijo & Team</li>
				<li>Hogan Lovells LLP</li>
				<li>JoAnn (Roberto Luongo) DeNapoli Charitable Foundation, Inc.</li>
				<li>New England Biolabs, Inc.</li>
				<li>Truro Vineyards and South Hollow Spirits</li>
			</ul>
		</div>
	</div>
</section>