<section id="video-chat" class="no-pad-bottom">
<div class="row">
	<div class="column large break">
		<section class="video-section menu-section no-pad-bottom no-pad-top" id="live-event">
			<h2 class="section-title">Live Event</h2>
				<div style="padding:56.25% 0 0 0; margin: 0 40px 0px 40px; position:relative;"><iframe src="<?php block_field( 'video-link' ); ?>" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>
				<p class="centered">If you would prefer to watch the video full screen, you can still use your phone to access the chat.</p>
				<p class="centered">Live tweeting the event? Use <strong>#VoicesoftheInnocent</strong> and tag <strong>@NEInnocence</strong></p>
		</section>
	</div>
	<div class="column small break">
		<section class="chat-box-section purple no-pad-bottom no-pad-top" id="chat">
			<h2 class="section-title">Chat</h2>
			<div class="container no-marg-bottom">
				<iframe src="<?php block_field( 'chat-link' ); ?>" style="width: 100%; height: 600px;"></iframe>
			</div>
		</section>
	</div>
</div>
</section>
