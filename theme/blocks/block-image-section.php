<section class="image-section">
	<div class="row icon-set">
		<div class="column three icon-box">
			<img src="<?php block_field( 'image-1' ); ?>"/>
		</div>
		<div class="column three icon-box">
			<img src="<?php block_field( 'image-2' ); ?>"/>
		</div>
		<div class="column three icon-box">
			<img src="<?php block_field( 'image-3' ); ?>"/>
		</div>
	</div>
</section>