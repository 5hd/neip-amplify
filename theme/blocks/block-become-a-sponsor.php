<section id="sponsorship" class="become-sponsor icon-section no-pad-bottom">
	<h2 class="section-title">Become a Sponsor</h2>
	<div class="row icon-set">
		<div class="column light-blue icon-box">
			<div class="icon">
				<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 432.31 281"><defs><style>.cls-1{fill:#fff}</style></defs><path class="cls-1" d="M90.35 493.77A43.35 43.35 0 00133.58 537h345.84a43.35 43.35 0 0043.23-43.23V382.93H90.35zM153.12 449h153.17v25.94H153.12zM479.42 256H133.58a43.35 43.35 0 00-43.23 43.23v23.17h432.3v-23.17A43.35 43.35 0 00479.42 256z" transform="translate(-90.35 -256)"/></svg>
			</div>
			<a href="https://secure.actblue.com/donate/voisponsorship" class="button" target="popup" onclick="window.open('https://secure.actblue.com/donate/voisponsorship','popup','width=600,height=600'); return false;">Sponsor Now</a>
		</div>
		<div class="column dark-blue icon-box">
			<div class="icon">
				<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 443.79 388.66"><defs><style>.cls-1{fill:#fff}</style></defs><path class="cls-1" d="M387.8 371.15a68 68 0 10-68-68 68 68 0 0068 68zm-4.8-62.91c-11.57-3.69-19.46-8.81-19.46-18.74 0-9.11 6.73-16.17 18.27-18.21v-9.73h10.59v9a41 41 0 0115.73 3.44l-3.23 11.56a35.22 35.22 0 00-15.58-3.59c-7 0-9.27 3-9.27 5.84 0 3.28 3.77 5.53 13.05 8.71 12.89 4.3 18 9.92 18 19.24 0 9.1-6.71 16.88-19.15 18.83v10.54h-10.7v-9.72c-7.25-.31-14.27-2.26-18.37-4.42l3.27-12a39.44 39.44 0 0017.85 4.5c6.27 0 10.48-2.35 10.48-6.36S391 310.8 383 308.24zM272 266.23a32.12 32.12 0 10-32.12-32.11A32.11 32.11 0 00272 266.23z" transform="translate(-84.43 -202)"/><path class="cls-1" d="M521 364c-11.62-10.45-27.6-7.57-38.87 4.27-12 12.55-79.12 83.25-79.12 83.25-9 9.16-19.52 18.29-36 18.29h-73.5c-6.65 0-13.74-5.25-13.74-13.42a13.44 13.44 0 0113.74-13.65h67.41c11.58 0 23.84-9.33 23.84-24s-12.45-23.91-25-23.91H230.35c-29.73 0-45.22 9.81-67 29.26-4.86 4.35-11.12 9.89-17.7 15.71l81.86 88.54a16.35 16.35 0 0110.09-3h116.13c27.78 0 53-3.34 76.29-28.29 22.83-24.46 76.91-83 89.72-96.95 9.44-10.29 12.53-25.96 1.26-36.1zM128.92 443a8.17 8.17 0 00-11.55-.46l-30.32 28a8.18 8.18 0 00-.45 11.55l99.89 108.05 11.55.45 30.32-28a8.17 8.17 0 00.45-11.55z" transform="translate(-84.43 -202)"/></svg>
			</div>
			<a class="button launch-form show">Make a Pledge</a>
			<?php get_template_part( 'template-parts/content', 'modal-form-sponsors' );?>
		</div>
	</div>
</section>