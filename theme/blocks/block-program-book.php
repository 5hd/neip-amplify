<section class="program-book menu-section" id="program-book">
	<h2 class="section-title">Program Book</h2>
	<div class="container centered white no-marg-bottom">
		<div style="position:relative;padding-top:max(60%,326px);height:0;width:100%"><iframe sandbox="allow-top-navigation allow-top-navigation-by-user-activation allow-downloads allow-scripts allow-same-origin allow-popups allow-modals allow-popups-to-escape-sandbox" allowfullscreen="true" style="position:absolute;border:none;width:100%;height:100%;left:0;right:0;top:0;bottom:0;" src="<?php block_field( 'program-book-link' ); ?>"></iframe></div>

		<p style="max-width:900px; margin:auto; margin-top:20px;">For more information, contact Jordan Salvatoriello at <br><a href="mailto:jordan@newenglandinnocence.org.">jordan@newenglandinnocence.org.</a></p>
	</div>
</section>