<section id="newsletter" class="icon-section no-pad-bottom">
	<h2 class="section-title">Other Ways to Get Involved</h2>
	<div class="row icon-set">
		<div class="column light-blue icon-box">
		<a href="https://www.newenglandinnocence.org/subscribe" class="button" target="popup" onclick="window.open('https://www.newenglandinnocence.org/subscribe','popup','width=600,height=600'); return false;">Subscribe to Our E-Newsletter</a>
		</div>
		<div class="column dark-blue icon-box">
	<a href="https://runningforinnocence.com/pick-an-event/" target="_blank" class="button">Learn More About <br>& Join the Running for Innocence Team</a>
		</div>
	</div>
</section>